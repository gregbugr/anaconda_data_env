Add conda-forge channel:
```
$ conda config --add channels conda-forge
```

Check:
```
$ conda config --show channels

channels:
	- conda-forge
	- defaults
```

Add required lab extensions
```
jupyter labextension install @jupyter-widgets/jupyterlab-manager@2.0.0
jupyter labextension install jupyterlab-plotly@4.6.0
jupyter labextension install plotlywidget@4.6.0
```

Check:
```
$ jupyter labextension list

JupyterLab v2.3.0
	Known labextensions:                                                
		app dir: C:\Users\greg\anaconda3\envs\data_env\share\jupyter\lab
			@jupyter-widgets/jupyterlab-manager v2.0.0 enabled  ok
			jupyterlab-plotly v4.6.0 enabled  ok
			plotlywidget v4.6.0 enabled  ok
```
